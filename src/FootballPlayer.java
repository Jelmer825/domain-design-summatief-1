public class FootballPlayer extends Player{

    public FootballPlayer(String name, int age) {
        super(name, age);
    }

    public double shootBall(Ball ball){
        return super.getAge() * ball.play() * 0.2;
    }

    @Override
    double playBall(Ball ball) {
        if(super.getAge() > 35){
            return shootBall(ball) * 0.1;
        }
        return shootBall(ball);
    }
}
