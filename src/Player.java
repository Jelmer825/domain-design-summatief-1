public abstract class Player {

    private final String name;
    private final int age;
    private Ball ball;

    public Player(String name, int age){
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setBall(Ball ball) {
        this.ball = ball;
    }

    public Ball getBall() {
        return this.ball;
    }

    abstract double playBall(Ball ball);

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", ball=" + ball +
                '}';
    }
}
