public class Main {
    public static void main(String[] args) {
        BaseballPlayer player1 = new BaseballPlayer("Jelmer", 60);
        FootballPlayer player2 = new FootballPlayer("Pieter", 16);

        Ball ball1 = new Baseball();
        Ball ball2 = new Football();

        player1.setBall(ball1);
        player2.setBall(ball2);

        double shot1 = player1.playBall(player1.getBall());
        double shot2 = player2.playBall(player2.getBall());

        System.out.println(player1.getName() + " Threw the ball " + shot1 + " Meters");
        System.out.println(player2.getName() + " Shot the ball " + shot2 + " Meters");

        winner(shot1, shot2);
    }


    public static void winner(double shot1, double shot2){
        // The person who shoots furthest wins
        if(shot1 == shot2){
            System.out.println("Draw!");
        }
        else if(shot1 > shot2){
            System.out.println("Person 1 wins!");
        }
        else if(shot1 < shot2){
            System.out.println("Person 2 wins!");
        }
        else{
            System.out.println("No winner?!");
        }

    }
}
