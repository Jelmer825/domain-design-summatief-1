public class BaseballPlayer extends Player {

    public BaseballPlayer(String name, int age) {
        super(name, age);
    }

    public double throwBall(Ball ball){
        return super.getAge() * ball.play() * 0.2;
    }

    @Override
    double playBall(Ball ball) {
        if (super.getAge() > 45) {
            return throwBall(ball) * 0.1;
        }
        return throwBall(ball);
    }
}
